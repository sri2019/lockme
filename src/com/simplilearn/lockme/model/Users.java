package com.simplilearn.lockme.model;

public class Users {
	
	private String username;
	private String password;
	  
	public Users() {}    // Zero argument constructor
	
	public Users(String username, String password) {    // Parameterized constructor
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Users [username=" + username + ", password=" + password + "]";
	}
	
	

}
